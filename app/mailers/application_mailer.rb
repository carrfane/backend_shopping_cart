class ApplicationMailer < ActionMailer::Base
  default from: 'emethodo@gmail.com'
  layout 'mailer'
end
