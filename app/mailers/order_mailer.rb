class OrderMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_mailer.confirm.subject
  #
  def confirm(order_id)
    @order = Order.find(order_id)
    delivery_options = { user_name: @order.company.smtp_user,
                         password: @order.company.smtp_password,
                         address: @order.company.smtp_host,
                         port: 587,
                         authentication: :plain,
                         domain: @order.company.smtp_user.split('@')[1]
                        }
    mail from: @order.company.smtp_user,to: @order.user.email, bcc: @order.company.admin_emails, subject: "#{@order.company.name} - Confirmación de Pedido", delivery_method_options: delivery_options
  end
  def delivery_time(order_id)
    @order = Order.find(order_id)
    delivery_options = { user_name: @order.company.smtp_user,
                         password: @order.company.smtp_password,
                         address: @order.company.smtp_host,
                         port: 587,
                         authentication: :plain,
                         domain: @order.company.smtp_user.split('@')[1]
                        }
    mail from: @order.company.smtp_user,to: @order.user.email, subject: "#{@order.company.name} - Tiempo espera pedido", delivery_method_options: delivery_options
  end
  def decline(params)
    @params = params
    @order = Order.find(params[:id])
    @operador = params[:operador]
    subject = params[:subject] || "#{@order.company.name} - Pedido rechazado"
    delivery_options = { user_name: @order.company.smtp_user,
                         password: @order.company.smtp_password,
                         address: @order.company.smtp_host,
                         port: 587,
                         authentication: :plain,
                         domain: @order.company.smtp_user.split('@')[1]
                        }
    mail from: @order.company.name,to: @order.user.email, bcc: params[:correo_copia], subject: subject, delivery_method_options: delivery_options
  end
  def decline_local(params)
    @params = params
    @order = Order.find(params[:id])
    @operador = params[:operador]
    subject = params[:subject] || "#{@order.company.name} - Pedido rechazado"
    delivery_options = { user_name: @order.company.smtp_user,
                         password: @order.company.smtp_password,
                         address: @order.company.smtp_host,
                         port: 587,
                         authentication: :plain,
                         domain: @order.company.smtp_user.split('@')[1]
                        }
    mail from: @order.company.name,to: @order.company.admin_emails, subject: subject, delivery_method_options: delivery_options
  end

  def decline_webpay(order_id)
    @order = Order.find(order_id)
    subject = params[:subject] || "#{@order.company.name} - Pedido rechazado por webpay"
    delivery_options = { user_name: @order.company.smtp_user,
                         password: @order.company.smtp_password,
                         address: @order.company.smtp_host,
                         port: 587,
                         authentication: :plain,
                         domain: @order.company.smtp_user.split('@')[1]
                        }
    mail from: @order.company.name,to: @order.company.admin_emails, subject: subject, delivery_method_options: delivery_options
  end

  def webpay(order_id)
    @order = Order.find(order_id)
    delivery_options = { user_name: @order.company.smtp_user,
                         password: @order.company.smtp_password,
                         address: @order.company.smtp_host,
                         port: 587,
                         authentication: :plain,
                         domain: @order.company.smtp_user.split('@')[1]
                        }
    mail from: @order.company.smtp_user,to: @order.company.admin_emails, subject: "#{@order.company.name} - Comprobante Webpay", delivery_method_options: delivery_options
  end
end