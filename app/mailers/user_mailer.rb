class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_recovery.subject
  #
  def password_recovery(user_id)
    @user = User.find(user_id)
    delivery_options = { user_name: @user.company.smtp_user,
                         password: @user.company.smtp_password,
                         address: @user.company.smtp_host,
                         port: 587,
                         authentication: :plain,
                         domain: @user.company.smtp_user.split('@')[1]
                        }
    mail from: @user.company.smtp_user,to: @user.email, subject: "#{@user.company.name} - Recuperación de Contraseña", delivery_method_options: delivery_options
  end
end
