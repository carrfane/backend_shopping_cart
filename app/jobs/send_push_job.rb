class SendPushJob < ApplicationJob
  queue_as :default

  def perform(channel,action,data)
    Pusher.trigger(channel,action,data)
  end
end
