class SyncJob < ApplicationJob
  queue_as :default
  def perform(url,method,data)
    full_url = url
    if method=='POST'
      result = HTTParty.post(full_url,{
        body: data.to_json,
        :headers => {
          'Content-Type' => 'application/json',
          'Accept' => 'application/json',
        }
      })
      if result.code == 200
        if data[:sell_id]
          order = Order.find(data[:sell_id])
          if order
            order.update_column(:synced,true)
          end
        end
      else
        throw "ERROR SYNCING"
      end
    end
  end
end
