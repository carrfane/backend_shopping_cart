class SyncDevelopmentJob < ApplicationJob
  queue_as :default

  rescue_from(RuntimeError) do
    retry_job(wait: 1.minute)
  end

  def perform(url,data,rut)
    full_url = url
      result = HTTParty.post(full_url,{
        body: data.to_json,
        :headers => {
          'Content-Type' => 'application/json',
          'Accept' => 'application/json',
          'Rut' => rut,
        }
       })
      if result.code == 200
        if data[:sell_id]
          order = Order.find(data[:sell_id])
          if order
            order.update_column(:synced,true)
          end
        end
      else
        raise "ERROR SYNCING"
        #throw "ERROR SYNCING"
      end
  end
end