class CompanyController < ApplicationController
  before_action :validate_company
  def index
    render json: @company.get_data
  end
end
