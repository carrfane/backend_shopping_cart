class OrdersController < ApplicationController
  before_action :validate_company, only: [:create,:index,:discount]
  before_action :validate_user, except: [:create,:show,:wait, :decline, :discount, :status]
  before_action :set_user, only: [:create]

  before_action :set_order, only: [:update, :destroy,:wait, :decline]

  # GET /orders
  def index
    @orders = @current_user.orders.where("(payment='webpay' AND payed = true) OR payment != 'webpay'").order("created_at DESC")

    render json: @orders, only: [:id,:items,:payment,:payed,:created_at], methods: [:total]
  end

  # GET /orders/1
  def show
    @order = Order.find_by_token(params[:id])
    render json: @order, only: [:id,:items,:payment,:payed,:token,:send_type,:discount,:webpay_data,:shop_id,:delivery], methods: [:total]
  end

  # POST /orders
  def create
    @order = Order.new(order_params)
    @order.user_id = @current_user.id
    @order.items = params[:order][:items]
    if params[:order] && params[:order][:address_id] && params[:order][:address_id]!=""
      if @current_user.fake
        @order.address_id = params[:order][:address_id]
      else
        @order.address = Address.find(params[:order][:address_id])
      end
    end
    if @order.save
      @order.update(company_id: @company.id)
      if @order.payment == 'webpay' && !@order.payed
        webpay = @order.webpay_start(ENV["API_URL"] || request.base_url,@current_user)
        if webpay["error_desc"]=="TRX_OK"
          render json: webpay, status: :created
        else
          OrderMailer.decline_webpay(@order.id).deliver_later
          render json: @order, status: :unprocessable_entity
        end
      else
        @order.notify
        render json: @order, status: :created
      end
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /orders/1
  def update
    if @order.update(order_params)
      render json: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end
  def end
    if Rails.env.development?
      order = @current_user.orders.find(params[:id])
      redirect_to "http://localhost:4200/orden/#{order.token}"
    elsif Rails.env.production?
      begin
        order = @current_user.orders.find(params[:id])
        redirect_to "#{order.user.company.domain}/orden/#{order.token}"
      rescue
        redirect_to "http://www.methodo.cl"
      end
    end
  end
  def result
    begin
      order = @current_user.orders.find(params[:id])
      if order && !order.payed
        result = MethodoWebpay.get_normal(order,params[:token_ws])
        order.update_attribute(:webpay_data,result)
        if result["error_desc"]=="TRX_OK"
          order.notify
          redirect_to result['urlredirection'].to_s+"?token_ws=#{params[:token_ws]}"
        else
          if Rails.env.development?
            redirect_to "http://localhost:4200/orden/#{order.token}"
          elsif Rails.env.production?
            begin
              redirect_to "#{order.user.company.domain}/orden/#{order.token}"
            rescue
              redirect_to "http://www.methodo.cl"
            end
          end
        end
      elsif order && order.payed
        if Rails.env.development?
          redirect_to "http://localhost:4200/orden/#{order.token}"
        elsif Rails.env.production?
          begin
            redirect_to "#{order.user.company.domain}/orden/#{order.token}"
          rescue
            redirect_to "http://www.methodo.cl"
          end
        end
      else
        render plain: "Not found", status: 404
      end
    rescue => error
      render json: error
    end
  end
  def test
    render status: :ok
  end
  def discount
    if @current_user
      url = "#{ENV["host"]}/web_services/discount?codigo=#{params[:code].gsub(/[^0-9a-z ]/i, '')}&email=#{@current_user.email}&company=#{@company.rut}"
    else
      url = "#{ENV["host"]}/web_services/discount?codigo=#{params[:code].gsub(/[^0-9a-z ]/i, '')}&company=#{@company.rut}"
    end
    begin
      render json: HTTParty.get(url)
    rescue
      render head: :nothing
    end
  end
  # DELETE /orders/1
  def destroy
    @order.destroy
  end
  def wait
    if params[:time]
      @order.time = params[:time]
      @order.save
    end
    @order.send_time
    render plain: "OK", status: 200
  end
  def decline
    format_params = {
      id: params[:id],
      reason: params[:reason],
      correo_copia: params[:correo_copia],
      subject: params[:subject],
      operador: params[:operador]
    }
    @order.send_decline(format_params)
    render plain: "OK", status: 200
  end

  def status
    render plain: "OK"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
      if !@order
        render status: :forbidden
      end
    end
    def verify_user
      
    end
    def set_user
      begin
        order = params[:order]
        @current_user = User.where(email: order[:email]).last
        if !@current_user && params[:order][:user_token]=="fake"
          @current_user = User.new({
            name: order[:name],
            email: order[:email],
            phone: order[:phone],
            fake: true,
            company_id: @company.methodo_id,
            password: 'not-registered-user',
            password_confirmation: 'not-registered-user'
          })
          if @current_user.save
            SyncJob.perform_now("#{@current_user.company.endpoint}/RecibeUsuario.php?cadena=#{@current_user.company_id}",'POST',{
              name: @current_user.name,
              email: @current_user.email,
              phone: @current_user.phone,
              facebook_id: @current_user.facebook_id || "-",
              google_id: @current_user.google_id || "-",
              user_token: @current_user.user_token,
              calle: "",
              numero: "",
              depto: "",
              block: "",
              comuna: "",
              ciudad: "",
              registrado: (@current_user.fake ? "0" : "1")
            })
          else
            render status: :forbidden
          end
        else
          @current_user.name = order[:name]
          @current_user.phone = order[:phone]
          @current_user.save
        end
      rescue => error
        puts error
        render status: :forbidden
      end
    end

    # Only allow a trusted parameter "white list" through.
    def order_params
      params.require(:order).permit(:send_type, :shop_id, :payment,:payment_amount, :discount_code, :delivery)
    end
end
