class AddressesController < ApplicationController
  before_action :validate_user, except: [:create]
  before_action :set_address, only: [:show, :update, :destroy]

  # GET /addresses
  def index
    @addresses = @current_user.addresses.all

    render json: @addresses
  end

  # GET /addresses/1
  def show
    render json: @address
  end

  # POST /addresses
  def create
    @address = Address.new(address_params)
    if params[:token]
      @current_user = User.find_by_user_token(params[:token])
      if @current_user
        @address.user_id = @current_user.id
      end
    end

    if @address.save
      render json: @address, status: :created, location: @address
    else
      render json: @address.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /addresses/1
  def update
    if @address.update(address_params)
      render json: @address
    else
      render json: @address.errors, status: :unprocessable_entity
    end
  end

  # DELETE /addresses/1
  def destroy
    @address.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address
      @address = @current_user.addresses.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def address_params
      params.require(:address).permit(:user_id, :calle, :numero, :depto, :block, :comuna, :ciudad, :latitude, :longitude)
    end
end
