class UsersController < ApplicationController
  before_action :find_user, only: [:create]
  before_action :validate_company
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  def index
    @users = @company.users.all

    render json: @users
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    if @user != nil
      render json: @user
    else
      @user = @company.users.new(user_params)
      if @user.save
        render json: @user, status: :created
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  def recover
    user = @company.users.find_by(email: params[:email].downcase, company_id: @company.methodo_id, fake: false)
    if user
      user.recover
      render status: 200
    else
      render status: 404
    end
  end
  def change_password
    if params[:token] && params[:token].length > 6
      user = @company.users.find_by(recovery_token: params[:token], company_id: @company.methodo_id, fake: false)
    end
    if user
      user.password = params[:password]
      user.password_confirmation = params[:password_confirmation]
      user.recovery_token = nil
      user.save
      render status: 200
    else
      render status: 404
    end
  end

  def login
    if params[:email] && params[:password]
      user = @company.users.find_by(email: params[:email].downcase, company_id: @company.methodo_id, fake: false)
      # If the user exists AND the password entered is correct.
      if user && user.authenticate(params[:password])
        # Save the user id inside the browser cookie. This is how we keep the user
        # logged in when they navigate around our website.
        render json: user
      else
      # If user's login doesn't work, send them back to the login form.
        render status: 401
      end
    elsif params[:facebook_token]
      response = HTTParty.get("https://graph.facebook.com/v2.8/me?access_token=#{params[:facebook_token]}&fields=email,name")
      id = JSON.parse(response.body)["id"]
      user = @company.users.find_by(facebook_id: id)
      if user
        render json: user
      else
        render status: 401, json: response
      end
    elsif params[:google_token]
      response = HTTParty.get("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=#{params[:google_token]}")
      id = JSON.parse(response.body)["id"]
      user = @company.users.find_by(google_id: id)
      if user
        render json: user
      else
        render status: 401, json: response
      end
    end
  end
  def check_email
    users = @company.users.where(fake: false, email: params[:email].downcase)
    render json: {used: users.any?}, status: 200
  end
  def check_phone
    users = @company.users.where(fake: false, phone: params[:phone].downcase)
    render json: {used: users.any?}, status: 200
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def find_user
      @user = User.find_by(email: params[:email])
    end

    def set_user
      @user = @company.users.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :phone, :facebook_id, :google_id, :user_token, :company_id)
    end
end
