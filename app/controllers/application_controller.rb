class ApplicationController < ActionController::API
  before_action :cors_preflight_check
  after_action :cors_set_access_control_headers

  # For all responses in this controller, return the CORS access control headers.
  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, PUT, PATCH, DELETE'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  # If this is a preflight OPTIONS request, then short-circuit the
  # request, return only the necessary headers and return an empty
  # text/plain.

  def cors_preflight_check
    if request.method == 'OPTIONS'
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, PUT, PATCH, DELETE'
      headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'
      headers['Access-Control-Max-Age'] = '1728000'
      render :plain => 'OK', :content_type => 'text/plain', status: 200
    end
  end
  def validate_user
    @current_user = User.find_by(user_token: params[:token])
    if !@current_user
      render status: :forbidden
    end
  end
  def validate_company
    companies = Company.where("domain ILIKE ?","%"+request.origin.to_s+"%")
    if companies.any? && request.origin
      @company = companies.last
    else
      render plain: "Methodo API WEBSERVICE #{DateTime.now}", status: 404
    end
  end
  def validate
    if params[:token]

    elsif params[:cadena]
      render status: 200
    end
  end
end
