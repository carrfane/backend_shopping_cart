# == Schema Information
#
# Table name: orders
#
#  id             :integer          not null, primary key
#  send_type      :string
#  shop_id        :integer
#  user_id        :integer
#  payment        :string
#  payed          :boolean          default("false")
#  synced         :boolean          default("false")
#  items          :json             default("{}")
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  payment_amount :integer          default("0")
#  webpay_data    :json
#  address_id     :integer
#  email          :string
#  name           :string
#  phone          :string
#  token          :string
#  discount_code  :string
#  discount       :integer
#  time           :integer          default("60")
#  delivery       :integer
#  company_id     :integer
#
# Indexes
#
#  index_orders_on_company_id  (company_id)
#



class Order < ApplicationRecord
  SEND_TYPES = ['local','delivery']
  PAYMENT_TYPES = ['webpay','oneclick','redcompra','cash','local']
  belongs_to :user, optional: true
  belongs_to :address, optional: true
  belongs_to :company, optional: true
  validates_presence_of :send_type, :shop_id, :user_id, :payment, :items
  validates_inclusion_of :send_type, in: SEND_TYPES
  validates_inclusion_of :payment, in: PAYMENT_TYPES
  before_create :set_token, :set_discount
  before_validation :set_discount
  #validate :equal_prices
  def equal_prices
    if self.total!= self.web_total
      errors.add(:base,"Los precios han cambiado, actualiza la página")
    end
  end
  def total
    #value = self.items.map{|i| self.get_item_total(i)}.sum
    company = self.company
    data = ( company.present? && company.data.present? ) ? company.data : nil
    totalDiscoun = 0
    totalNoDiscoun = 0
    if data.present?
      self.items.each do |item|
        product = data['products'].select{|product| product["id_producto"].to_i == item['id_producto'].to_i}
        if product.present?
          family = data['categories'].select{|category| category["id_familia"].to_i == product[0]["id_familia"].to_i and category["fam_max_descuento"].to_i == 0}
          if family.present?
            totalNoDiscoun += self.get_item_total(item)
          else
            totalDiscoun += self.get_item_total(item)
          end
        end
      end
    end
    value = ((totalDiscoun*(100-self.discount.to_i))/100).round(0) + totalNoDiscoun.round(0)
    if self.send_type == 'delivery'
      begin
        value += self.delivery || self.local['despacho'] || 1990
      rescue
        value += 0
      end
    end
    return value
  end
  def web_total
    value = self.items.map{|i| i['amount'].to_i}.sum
    value = ((value*(100-self.discount.to_i))/100).round(0)
    if self.send_type == 'delivery'
      begin
        value += self.delivery || self.local['despacho'] || 1990
      rescue
        value += 0
      end
    end
    return value
  end
  def send_email
    if self.payed || self.payment!='webpay'
      OrderMailer.confirm(self.id).deliver_later
    end
  end
  def send_time
    OrderMailer.delivery_time(self.id).deliver_later
  end
  def send_decline(params)
    OrderMailer.decline(params).deliver_later
    OrderMailer.decline_local(params).deliver_later
  end
  def locales
    self.company.data['locales']
  end
  def local
    begin
      index = self.company.data['locales'].index{|local| local['id_local'].to_i == self.shop_id}
      self.company.data['locales'][index]
    rescue
      {}
    end
  end
  def get_item_total(item)
    begin
      index = self.company.data['products'].index{|product| product["id_producto"].to_i==item['id_producto'].to_i}
      amount = self.company.data['products'][index]['costo_internet'] || self.company.data['products'][index]['costo']
      amount = amount.to_i*item['cantidad'].to_i
      if item['families'] && items['families']['items']
        item['families'] && items['families']['items'].each do |k,v|
          v.each do |prod|
            amount += (prod['costo_extra'] || 0).to_i
          end
        end
      end
      return amount
    rescue => error
      return item['amount']
    end
  end
  def get_item_name(id)
    begin
      index = self.company.data['products'].index{|product| product["id_producto"].to_i==id.to_i}
      if index.present?
        nombre = self.company.data['products'][index]['pro_nombre']
      else
        i = self.company.data['accessories'].index{|product| product["id_producto"].to_i==id.to_i}
        nombre = self.company.data['accessories'][i]['pro_nombre'] == "Palitos" ? "Personas" : self.company.data['accessories'][i]['pro_nombre']
      end
      nombre 
    rescue => error
      'Sin Nombre'
    end
  end
  def get_price(id)
    begin
      index = self.company.data['products'].index{|product| product["id_producto"].to_i==id.to_i}
      if index.present?
        price = self.company.data['products'][index]['costo_internet'] || self.company.data['products'][index]['costo']
      else
        i = self.company.data['accessories'].index{|product| product["id_producto"].to_i==id.to_i}
        price = self.company.data['accessories'][index]['costo_internet'] || self.company.data['accessories'][index]['costo']
      end
      price
    rescue => error
      0
    end
  end
  def set_discount
    if self.discount_code
      self.discount = self.get_discount
    end
  end
  def get_discount
    url = "#{ENV["host"]}/web_services/discount?codigo=#{self.discount_code}&company=#{self.company.rut}"
    begin
      response = JSON.parse(HTTParty.get(url).body)
      return response['porcentaje_descuento'].to_i
    rescue
      return 0
    end
  end
  def set_token
    self.token = OpenSSL::HMAC.hexdigest("sha256", SecureRandom.hex, self.id.to_s)
  end
  def webpay_start(url,user)
    MethodoWebpay.start_normal(self,url,user)
  end
  def notify
    if self.webpay_data && self.webpay_data['responsecode']=="0"
      self.update_attribute(:payed,true)
      self.payed = true
      OrderMailer.webpay(self.id).deliver_later
    end
    if Rails.env.production? && (self.payed || self.payment!='webpay')
      self.sync_development #self.sync
    end
    if Rails.env.development? && (self.payed || self.payment!='webpay')
      self.sync_development
    end
    self.send_email
  end
  def formattedSync
    productos = []
    self.items.each do |i|
      productos.push({
        "id_producto": i["id_producto"],
        "cantidad": i["cantidad"],
        "nombre": self.get_item_name(i["id_producto"]),
        "precio": self.get_price(i["id_producto"]),
        "id_padre": 0,

      })
      if i['families'] && i['families']['items']
        i['families']['items'].each do |k,v|
          v.each do |producto|
            productos.push({
              "id_producto": producto["id_producto"],
              "cantidad": producto["cantidad"] || 1,
              "nombre": producto["pro_nombre"],
              "precio": producto["costo_extra"] || producto["costo"],
              "id_padre": 0,
            })
          end
        end
      end
    end
    {
      sell_id: self.id,
      fecha: self.created_at.iso8601.split('T')[0],
      hora: self.created_at.iso8601.split('T')[1],
      tipo_pago: self.payment,
      pagado: (self.payed ? "1" : "0"),
      tipo_envio: (self.send_type == "delivery" ? "0" : "1"),
      monto: self.total,
      vuelto_monto: self.payment_amount.to_s,
      calle: self.address ? self.address.calle : "",
      numero: self.address ? self.address.numero : "",
      depto: self.address ? self.address.depto : "",
      descuento: self.discount_code,
      block: self.address ? self.address.block : "",
      comuna: self.address ? self.address.comuna : "",
      ciudad: self.address ? self.address.ciudad : "",
      detalle_venta: productos,
      telefono: self.user.phone,
      nombre: self.user.name,
      email: self.user.email,
      shop_id: self.shop_id,
    }
  end
  def sync
    SyncJob.perform_later("#{self.company.endpoint}/RecibePedido.php?email=#{self.user.email}&id_local=#{self.shop_id}&id_venta=#{self.id}",'POST',self.formattedSync)
  end

  def sync_development
    SyncDevelopmentJob.perform_later("#{ENV["host"]}/web_services/pedido_ecommerce",self.formattedSync, self.company.rut)
  end
end
