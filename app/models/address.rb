# == Schema Information
#
# Table name: addresses
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  calle      :string
#  numero     :string
#  depto      :string
#  block      :string
#  comuna     :string
#  ciudad     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  latitude   :string
#  longitude  :string
#
# Indexes
#
#  index_addresses_on_user_id  (user_id)
#



class Address < ApplicationRecord
  belongs_to :user, optional: true
  validates_presence_of :calle, :numero, :comuna, :ciudad
  validates_length_of :depto, :maximum => 10, :allow_blank => true, :message => 'El departamento tiene un maximo de 10 caracteres'
  validates_length_of :block, :maximum => 10, :allow_blank => true, :message => 'El block tiene un maximo de 10 caracteres'
  #validates_uniqueness_of :calle, :numero, :depto, scope: :user_id
  geocoded_by :address
  after_validation :geocode
  validate :validate_address
  def address
    "#{calle} #{numero} #{depto} #{block}, #{comuna}, #{ciudad}"
  end

  def validate_address
    if self.user.present?
    	address = self.user.addresses.where.not(id: self.id)
    	address.each do |a|
    		if (a.address == self.address)
    			errors[:base] << ("La dirección ya se encuentra en uso.")
    		end
    	end
    end
  end
end
