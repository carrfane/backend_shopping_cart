# == Schema Information
#
# Table name: companies
#
#  id            :integer          not null, primary key
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  methodo_id    :integer
#  data          :json             default("{}")
#  endpoint      :string
#  domain        :string
#  smtp_user     :string
#  smtp_password :string
#  smtp_host     :string
#  web           :string
#  twitter       :string
#  instagram     :string
#  facebook      :string
#  commerce_code :string
#  environment   :string
#  public_cert   :text
#  private_key   :text
#  webpay_cert   :text
#  email         :string
#  admin_emails  :string
#  custom_script :text
#  rut           :string
#  google        :text
#




class Company < ApplicationRecord
  validates_presence_of :name, :endpoint, :methodo_id, :domain, :web, :smtp_user, :smtp_host, :smtp_password, :rut
  validates_uniqueness_of :methodo_id
  has_many :users, :primary_key => :methodo_id
  has_many :orders
  rails_admin do
    exclude_fields :created_at, :updated_at, :data
    edit do
      field :public_cert, :froala
      field :private_key, :froala
      field :webpay_cert, :froala
    end
  end
  def get_data
    begin
      response =  HTTParty.get("#{ENV["host"]}/web_services/ecommerce?cadena=#{self.methodo_id}&local=todos", #HTTParty.get("#{self.endpoint}?cadena=#{self.methodo_id}&local=todos",
        :headers => {
          'Content-Type' => 'application/json',
          'Accept' => 'application/json',
          'Rut' => self.rut
        }
      )
      if response.code==200
        self.update_column(:data,response)
        return self.send_data
      else
        return self.send_data
      end
    rescue => error
      puts error
      return self.send_data
    end
  end
  def send_data
    values = self.data
    values['custom_script'] = self.custom_script
    values['web'] = self.web
    values['facebook'] = self.facebook
    values['twitter'] = self.twitter
    values['instagram'] = self.instagram
    values['server_time'] = DateTime.now
    values['has_webpay'] = (self.public_cert && self.private_key && self.webpay_cert && self.commerce_code && self.environment) || Rails.env.development?
    values['google_analytics'] = self.google
    return values
  end
end
