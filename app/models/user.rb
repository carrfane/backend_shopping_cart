# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string
#  email           :string
#  phone           :string
#  facebook_id     :string
#  google_id       :string
#  user_token      :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  password_digest :string
#  company_id      :integer
#  synced          :boolean          default("false")
#  fake            :boolean          default("false")
#  recovery_token  :string
#


class User < ApplicationRecord
  has_many :orders
  has_many :addresses
  belongs_to :company, :primary_key => :methodo_id
  validates_presence_of :name, :email, :phone, :company_id
  validates_uniqueness_of :email, scope: [:company_id,:fake], if: Proc.new {|u| !u.fake}
  has_secure_password
  before_create :set_user_token
  after_commit :sync, on: [:create], if: Proc.new {|u| !u.fake}
  def set_user_token
    self.email = self.email.downcase
    self.user_token = OpenSSL::HMAC.hexdigest("sha256", SecureRandom.hex, self.id.to_s)
  end
  def recover
    self.recovery_token = OpenSSL::HMAC.hexdigest("sha256", SecureRandom.hex, self.id.to_s)
    if self.save
      UserMailer.password_recovery(self.id).deliver_later
    else
      puts self.errors.inspect
    end
  end
  def methodo_id
    self.company_id
  end
  def sync
    if Rails.env.production?
      SyncJob.perform_later("#{self.company.endpoint}/RecibeUsuario.php?cadena=#{self.company_id}",'POST',{
        name: self.name,
        email: self.email,
        phone: self.phone,
        facebook_id: self.facebook_id || "-",
        google_id: self.google_id || "-",
        user_token: self.user_token,
        calle: "",
        numero: "",
        depto: "",
        block: "",
        comuna: "",
        ciudad: "",
        registrado: (self.fake ? "0" : "1")
      })
    end
  end
end
