

class MethodoWebpay
def self.webpay(company=nil)
    require_relative './certificates/cert_normal'
    #Se crea objeto para invocar la libreria
    libwebpay = Libwebpay.new
    #se rescatan variables de los certificados
    certificates = CertNormal.new
    config = libwebpay.getConfiguration
    if company
      config.commerce_code = (company.commerce_code || certificates.commerce_code)
      config.environment = (company.environment || certificates.environment)
      config.private_key = (company.private_key || certificates.private_key)
      config.public_cert = (company.public_cert || certificates.public_cert)
      config.webpay_cert = (company.webpay_cert || certificates.webpay_cert)
    else
      config.commerce_code = certificates.commerce_code
      config.environment = certificates.environment
      config.private_key = certificates.private_key
      config.public_cert = certificates.public_cert
      config.webpay_cert = certificates.webpay_cert
    end
    return libwebpay.getWebpay(config)
  end
  def self.start_normal(order,url,user)
    urlReturn = url+"/orders/#{order.id}/result?token=#{user.user_token}&"
    urlFinal = url+"/orders/#{order.id}/end?token=#{user.user_token}&"
    result = self.webpay(order.company).getNormalTransaction.initTransaction(order.total, order.id, order.user_id, urlReturn, urlFinal)
  end
  def self.get_normal(order,token)
    result = self.webpay(order.company).getNormalTransaction.getTransactionResult(token)
  end
end
