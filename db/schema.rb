# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180904202722) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "calle"
    t.string   "numero"
    t.string   "depto"
    t.string   "block"
    t.string   "comuna"
    t.string   "ciudad"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "latitude"
    t.string   "longitude"
    t.index ["user_id"], name: "index_addresses_on_user_id", using: :btree
  end

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "methodo_id"
    t.json     "data",          default: {}
    t.string   "endpoint"
    t.string   "domain"
    t.string   "smtp_user"
    t.string   "smtp_password"
    t.string   "smtp_host"
    t.string   "web"
    t.string   "twitter"
    t.string   "instagram"
    t.string   "facebook"
    t.string   "commerce_code"
    t.string   "environment"
    t.text     "public_cert"
    t.text     "private_key"
    t.text     "webpay_cert"
    t.string   "email"
    t.string   "admin_emails"
    t.text     "custom_script"
    t.string   "rut"
    t.text     "google"
  end

  create_table "orders", force: :cascade do |t|
    t.string   "send_type"
    t.integer  "shop_id"
    t.integer  "user_id"
    t.string   "payment"
    t.boolean  "payed",          default: false
    t.boolean  "synced",         default: false
    t.json     "items",          default: {}
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "payment_amount", default: 0
    t.json     "webpay_data"
    t.integer  "address_id"
    t.string   "email"
    t.string   "name"
    t.string   "phone"
    t.string   "token"
    t.string   "discount_code"
    t.integer  "discount"
    t.integer  "time",           default: 60
    t.integer  "delivery"
    t.integer  "company_id"
    t.index ["company_id"], name: "index_orders_on_company_id", using: :btree
  end

  create_table "shops", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.string   "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "facebook_id"
    t.string   "google_id"
    t.string   "user_token"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.integer  "company_id"
    t.boolean  "synced",          default: false
    t.boolean  "fake",            default: false
    t.string   "recovery_token"
  end

  add_foreign_key "addresses", "users"
  add_foreign_key "orders", "companies"
end
