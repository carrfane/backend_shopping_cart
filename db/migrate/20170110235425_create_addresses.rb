class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.belongs_to :user, foreign_key: true
      t.string :calle
      t.string :numero
      t.string :depto
      t.string :block
      t.string :comuna
      t.string :ciudad

      t.timestamps
    end
  end
end
