class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password
      t.string :password_confirmation
      t.string :phone
      t.string :facebook_id
      t.string :google_id
      t.string :user_token

      t.timestamps
    end
  end
end
