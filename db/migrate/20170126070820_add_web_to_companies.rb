class AddWebToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :web, :string
  end
end
