class ModifyCompanyModel < ActiveRecord::Migration[5.0]
  def change
    remove_column :companies, :logo, :string
    remove_column :companies, :colors, :json
    add_column :companies, :methodo_id, :integer, index: true
    add_column :companies, :data, :json, default: {}
    add_column :companies, :endpoint, :string
  end
end
