class AddCustomScriptsToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :custom_script, :text
  end
end
