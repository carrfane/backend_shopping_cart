class AddWebpayDataToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :webpay_data, :json
  end
end
