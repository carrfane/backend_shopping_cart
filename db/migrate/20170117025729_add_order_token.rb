class AddOrderToken < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :token, :string
    Order.all.each do |o|
      o.set_token
      o.save
    end
  end
end
