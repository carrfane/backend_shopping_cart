class AddCompanyToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :synced, :boolean, default: false
  end
end
