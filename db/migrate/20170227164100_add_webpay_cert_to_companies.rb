class AddWebpayCertToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :commerce_code, :string
    add_column :companies, :environment, :string
    add_column :companies, :public_cert, :text
    add_column :companies, :private_key, :text
    add_column :companies, :webpay_cert, :text
  end
end
