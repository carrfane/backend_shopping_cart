class AddAdminMailsToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :admin_emails, :string
  end
end
