class AddDeliveryCostToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :delivery, :integer
  end
end
