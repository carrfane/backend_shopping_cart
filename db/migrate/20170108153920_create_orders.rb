class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.string :send_type
      t.integer :shop_id
      t.integer :user_id
      t.string :payment
      t.boolean :payed, default: false
      t.boolean :synced, default: false
      t.json :items, default: {}
      t.timestamps
    end
  end
end
