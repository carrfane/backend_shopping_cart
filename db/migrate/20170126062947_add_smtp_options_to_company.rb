class AddSmtpOptionsToCompany < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :smtp_user, :string
    add_column :companies, :smtp_password, :string
    add_column :companies, :smtp_host, :string
  end
end
