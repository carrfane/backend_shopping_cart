class AddTimeToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :time, :integer, default: 60
  end
end
