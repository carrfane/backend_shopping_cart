require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
# require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MethodoApi
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    # config.active_job.queue_adapter = :sidekiq
    config.middleware.use Rack::MethodOverride
    config.api_only = true
    config.time_zone = 'Santiago'
    config.active_record.default_timezone = :local
    # Email
    config.action_controller.default_url_options = {:host=>"webapi.methodo.cl",port: 80}
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    ActionMailer::Base.smtp_settings = {
      :address        => 'smtp.gmail.com',
      :port           => 587,
      :authentication => :plain,
      :user_name      => 'emethodo@gmail.com',
      :password       => 'Pass2020',
      :domain         => 'gmail',
    }
  end
end
