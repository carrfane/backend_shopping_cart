Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  mount RailsAdmin::Engine => '/manage', as: 'rails_admin'


  resources :addresses
  resources :orders, only: [:create,:index,:show] do
    member do
      post :end
      post :wait
      post :decline
      get :end
      post :result
    end
  end
  resources :company, only: [:index]
  resources :users, only: [:create]
  get '/auth' => 'users#login'
  get '/discount' => 'orders#discount'
  get '/check_email' => 'users#check_email'
  get '/check_phone' => 'users#check_phone'
  get '/check_status' => 'orders#status'
  root 'company#index'
  post 'recover' => 'users#recover'
  post 'changePassword' => 'users#change_password'
  match "*path", controller: "application", action: "validate", via: [:options]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
