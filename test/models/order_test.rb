# == Schema Information
#
# Table name: orders
#
#  id             :integer          not null, primary key
#  send_type      :string
#  shop_id        :integer
#  user_id        :integer
#  payment        :string
#  payed          :boolean          default("false")
#  synced         :boolean          default("false")
#  items          :json             default("{}")
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  payment_amount :integer          default("0")
#  webpay_data    :json
#  address_id     :integer
#  email          :string
#  name           :string
#  phone          :string
#  token          :string
#  discount_code  :string
#  discount       :integer
#  time           :integer          default("60")
#  delivery       :integer
#  company_id     :integer
#
# Indexes
#
#  index_orders_on_company_id  (company_id)
#


require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
