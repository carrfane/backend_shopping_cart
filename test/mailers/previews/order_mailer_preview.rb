# Preview all emails at http://localhost:3000/rails/mailers/order_mailer
class OrderMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/order_mailer/confirm
  def confirm
    @order = Order.last
    OrderMailer.confirm(@order.id)
  end
  # Preview this email at http://localhost:3001/rails/mailers/order_mailer/webpay
  def webpay
    @order = Order.where(payed: true).last
    OrderMailer.webpay(@order.id)
  end
  # Preview this email at http://localhost:3000/rails/mailers/order_mailer/delivery_time
  def delivery_time
    @order = Order.last
    OrderMailer.delivery_time(@order.id)
  end
  # Preview this email at http://localhost:3000/rails/mailers/order_mailer/decline
  def decline
    params = {
      id: Order.last.id,
      reason: "No hay ingredientes",
      correo_copia: "sebastian.paivam@gmail.com",
      subject: "Sujeto de prueba",
      name: "Roccoto"
    }
    OrderMailer.decline(params)
  end
  def decline_local
    params = {
      id: Order.last.id,
      reason: "No hay ingredientes",
      correo_copia: "sebastian.paivam@gmail.com",
      subject: "Sujeto de prueba",
      name: "Roccoto"
    }
    OrderMailer.decline_local(params)
  end

end
