# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/password_recovery
  def password_recovery
    @user = User.last
    UserMailer.password_recovery(@user.id)
  end

end
