# POST /orders
def create
  @order = Order.new(order_params)
  @order.user_id = @current_user.id
  @order.items = params[:order][:items]
  if params[:order] && params[:order][:address_id] && params[:order][:address_id]!=""
    if @current_user.fake
      @order.address_id = params[:order][:address_id]
    else
      @order.address = @current_user.addresses.find(params[:order][:address_id])
    end
  end
  if @order.save
    ##INICIAR WEBPAY
    if @order.payment == 'webpay' && !@order.payed
      render json: @order.webpay_start(request.base_url,@current_user), status: :created
    else
      @order.notify
      render json: @order, status: :created
    end
  else
    render json: @order.errors, status: :unprocessable_entity
  end
end
##FINALIZAR WEBPAY
def end
  if Rails.env.development?
    order = @current_user.orders.find(params[:id])
    redirect_to "http://localhost:4200/orden/#{order.token}"
  elsif Rails.env.production?
    begin
      order = @current_user.orders.find(params[:id])
      redirect_to "#{order.user.company.domain}/orden/#{order.token}"
    rescue
      redirect_to "http://www.methodo.cl"
    end
  end
end
##RESULTADO DE TRANSACCION
def result
  begin
    order = @current_user.orders.find(params[:id])
    ##BUSCAR RESULTADO WEBPAY SOLO SI NO ESTA PAGADO Y HACER AKNOWNLEDGE
    if order && !order.payed
      result = MethodoWebpay.get_normal(params[:token_ws])
      order.update_attribute(:webpay_data,result)
      order.notify
      redirect_to result['urlredirection'].to_s+"?token_ws=#{params[:token_ws]}"
    ##SI ESTA PAGADO REDIRIGIR A COMERCIO MOSTRAR PANTALLA DE RESULTADO DE ORDEN
    elsif order && order.payed
      if Rails.env.development?
        redirect_to "http://localhost:4200/orden/#{order.token}"
      elsif Rails.env.production?
        begin
          redirect_to "#{order.user.company.domain}/orden/#{order.token}"
        rescue
          redirect_to "http://www.methodo.cl"
        end
      end
    else
      render plain: "Not found", status: 404
    end
  rescue => error
    render json: error
  end
end


##CLASE WEBPAY UTILIZADA
class MethodoWebpay
  def self.webpay
    require_relative './certificates/cert_normal'
    #Se crea objeto para invocar la libreria
    libwebpay = Libwebpay.new
    #se rescatan variables de los certificados
    certificates = CertNormal.new
    config = libwebpay.getConfiguration
    config.commerce_code = certificates.commerce_code
    config.environment = certificates.environment
    config.private_key = certificates.private_key
    config.public_cert = certificates.public_cert
    config.webpay_cert = certificates.webpay_cert
    return libwebpay.getWebpay(config)
  end
  def self.start_normal(order,url,user)
    urlReturn = url+"/orders/#{order.id}/result?token=#{user.user_token}&"
    urlFinal = url+"/orders/#{order.id}/end?token=#{user.user_token}&"
    result = self.webpay.getNormalTransaction.initTransaction(order.total, order.id, order.user_id, urlReturn, urlFinal)
  end
  def self.get_normal(token)
    result = self.webpay.getNormalTransaction.getTransactionResult(token)
  end
end
